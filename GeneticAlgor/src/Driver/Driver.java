/**
 * 
 */
package Driver;

import java.io.File;

import BackEnd.*;

/**
 * @author Jigar Doshi, Carmine Iannaccone
 * 
 */
public class Driver {

	public static void main(String[] args) {

		// provide the pathname of directory with files
		//String pathName_20 = "C://Users//Jigar Doshi//git//geneticalgorithm3sat//GeneticAlgor//US50//";
		// array with all files in the folder
		
		String pathName = args[0];
		
		String[] filenames = visitAllDirsAndFiles(pathName);
		try {
			if (filenames.length == 0) {
				System.out.println("No files Exists");
				System.exit(-1);
			}
		} catch (Exception e) {
			System.out.println("Dir no found!");
			System.exit(1);
		}

		// Count the number of success and total test case
		int success = 0;
		int testCase = 0;
		
		int sizeList = filenames.length;
		
		if(sizeList >100){
			sizeList = 100;
		}
		
		System.out.println("TestCase"+"|"+ "Success");
		System.out.println("------------------");
		
		
		for (int i = 0; i < sizeList; i++) {

			String file = filenames[i];
			ReadFile r = new ReadFile(pathName + "" + file);
			r.readFromFile();
			
			GeneticAlgorithm genetic = new GeneticAlgorithm(r);
			boolean succ = true;

			long start = System.currentTimeMillis();
			long end = start + 60 * 1000;// 60 seconds * 1000 ms/sec
			
			boolean bsuc = false;
			
			// Every File is given 60 secs to find a solution, else it is considered Failed Case
			while (System.currentTimeMillis() < end){
				
				succ = genetic.fitnessTest();
				
				if (!succ) {
					success++;
					bsuc = true;
					break;
				}
				genetic.selection();
				genetic.crossover();
				genetic.mutation();
				genetic.flip();
			}
			testCase++;
			System.out.println("   "+testCase + "\t|  "+bsuc);
			bsuc = false;
		
			
		}
		
		
		System.out.print("------------------");
		System.out.print("\n");
		System.out.println("TOTAL TestCase = " + testCase);
		System.out.println("Total Success =\t"+ + success);
		System.out.println("Average flips =\t"+ + (GeneticAlgorithm.totalFlips/sizeList));
		System.out.println("Total failure =\t" + (testCase-success));
		System.out.println("Success Rate =\t" + ((double)success/(double)testCase)*100 + "%");
	
		  

	/*	Testing for one file
	 * String fileName = "uf75-01.cnf";
		ReadFile r = new ReadFile(fileName);

		r.readFromFile();
		// System.out.println(r.getclauses()+ " " + r.getvariables());
		// .testNodes();

		// System.exit(0);

		GeneticAlgorithm g_20 = new GeneticAlgorithm(r);
		boolean succ = true;

		int cycle = 0;
		
		System.out.println("Finding a solution....");
		
		while (succ) {
			succ = g_20.fitnessTest();
			g_20.selection();
			g_20.crossover();
			g_20.mutation();
			g_20.flip();
			cycle++;
		}

		System.out.println(cycle);
		System.out.println("Done!");*/

	}

	/*
	 * Method Traverses all sfiles in the directory
	 */
	public static String[] visitAllDirsAndFiles(String str) {

		File dir = new File(str);
		// System.out.println(dir);
		if (dir.isDirectory()) {
			String[] children = dir.list();
			return children;
		} else {
			return null;
		}

	}

}
