/**
 * 
 */
package BackEnd;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author Jigar Doshi
 *
 */

public class ReadFile {

	public String fileName;

	private int clauses, variables;
	Node[][] n = null;

	public ReadFile(String fileName) {
		this.fileName = fileName;
		
	}
	
	/*
	 * Reads from files and puts in 2D array called expression
	 */

	public void readFromFile() {

		int line = 0;

		try {
			Scanner in = new Scanner(new File(fileName));
			while (in.hasNext()) {

				String lineStr = in.nextLine();

				if (lineStr.charAt(0) == '%') {
					break;
				}

				if (lineStr.charAt(0) == 'c') {
					continue;
				}
				if (lineStr.charAt(0) == 'p') {
					String[] temp = lineStr.split(" ");
					variables = Integer.parseInt(temp[2]);
					clauses = Integer.parseInt(temp[4]);
					n = new Node[clauses][variables];
					continue;
				}

				String[] tempClause = lineStr.split(" ");
				for (String s : tempClause) {
					if (s.equals("0")) {
						break;
					}
					if (s.equals("")) {
						continue;
					}
					int tempVar = Integer.parseInt(s);

					if (tempVar > 0) {
						n[line][tempVar - 1] = new Node(true, false, false);
					} else {
						tempVar = Math.abs(tempVar);
						n[line][tempVar - 1] = new Node(true, false, true);
					}

				}
				line++;

			}

		} catch (FileNotFoundException e) {
			System.out.println(fileName + ": Not Found");
		}

	}

	/*
	 * Returns the Expression with clauses
	 */
	public Node[][] getExpersion() {
		return n;
	}

	/*
	 * Testing if nodes are created correctly
	 */
	public void testNodes() {

		// System.out.println("col"+ n[0].length + "row"+ n.length);
		for (int i = 0; i < n.length; i++) {
			for (int j = 0; j < n[0].length; j++) {
				if (n[i][j] != null) {
					if (!n[i][j].getNegation()) {
						System.out.print(j + 1 + " ");
					}else{
						System.out.print((j + 1)*-1 + " ");
					}
				}

			}
			System.out.print("\n");
		}

	}

	public int getclauses() {
		return this.clauses;
	}

	public int getvariables() {

		return this.variables;
	}

	public void setclauses(int clause) {
		this.clauses = clause;
	}

	public void setvariables(int variables) {

		this.variables = variables;
	}


}
