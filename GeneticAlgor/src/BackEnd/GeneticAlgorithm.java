/**
 * 
 */
package BackEnd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Jigar Doshi, Carmine Iannaccone
 * 
 */

// just made a small change
public class GeneticAlgorithm {

	public static int totalFlips = 0;
	int firstBest = 0, secondBest = 0;
	boolean[][] population, newPop, selection;
	private boolean goalFound = false;
	Node[][] Expersion;
	int numVar;
	double randomState;
	double[] fitness = new double[10];;
	public Node[][] expr;
	public ReadFile rFile;

	public GeneticAlgorithm(ReadFile rFile) {

		this.rFile = rFile;
		this.numVar = rFile.getvariables();
		expr = this.rFile.getExpersion();
		population = new boolean[10][numVar];
		newPop = new boolean[10][numVar];
		selection = new boolean[10][numVar];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < numVar; j++) {
				double rand = randDouble();
				if (rand > 0.5) {
					population[i][j] = true;
				} else {
					population[i][j] = false;
				}
			}
		}

	}
	
	/*
	 * Returns ture if the clause is satisfied
	 */

	public boolean satisfyClause(int row, boolean[] solution) {

		for (int i = 0; i < expr[0].length; i++) {
			if (expr[row][i] != null) {
				if (expr[row][i].getExists()) {
					// Node temp = expr[row][i];
					if (!expr[row][i].getNegation()) {
						if (solution[i]) {
							// System.out.println("yes");
							return true;
						}
					} else { // For variable with negation
						if (!solution[i]) {

							// System.out.println("yes");
							return true;
						}
					}
				}
			}
		}

		return false;

	}

	/*
	 * Returns the number of clauses met given a solution set
	 */
	public int numberOfClauseSatisfy(boolean[] solution) {

		int totalCount = 0;
		try {
			for (int i = 0; i < expr.length; i++) {
				if (satisfyClause(i, solution)) {
					totalCount++;
				}
			}
		} catch (Exception e) {
			System.out.println(rFile.fileName);
		}
		return totalCount;
	}

	public boolean goalSatate() {
		if (goalFound == true) {
			return true;
		}
		return false;
	}

	/*
	 * Fitness test for each population
	 */
	public boolean fitnessTest() {

		int totalSat = 0;
		int[] tempSat = new int[10];
		for (int i = 0; i < 10; i++) {
			// the number of clauses satisfied
			tempSat[i] = numberOfClauseSatisfy(population[i]);

			if (tempSat[i] == this.rFile.getclauses()) {
				//System.out.println("Found Solution!!!");
				return false;
			}

			totalSat += tempSat[i];
		}
		// Gives a fitness to each solution based on the clause satisfactory 
		for (int i = 0; i < 10; i++) {
			this.fitness[i] = tempSat[i] / (double) (totalSat);
		}

		// System.out.println("done with fitness");
		return true;

	}

	public void selection() {

		
		boolean[][] holder = new boolean[10][numVar];
		double[] ranges = new double[11];
		int selCounter = 0;
		ranges[0] = 0.0;
		
		// Normalization for probablistic selection
		
		for (int i = 1; i < 11; i++) {
			for (int j = 0; j < i; j++) {
				ranges[i] = ranges[i - 1] + this.fitness[j];

			}
		}

		// Chosing the two elites

		if (fitness[0] >= fitness[1]) {
			firstBest = 0;
			secondBest = 1;
		} else {
			firstBest = 1;
			secondBest = 0;
		}
		for (int y = 2; y < 10; y++) {
			if (fitness[y] > fitness[firstBest]) {
				secondBest = firstBest;
				firstBest = y;
			} else if ((fitness[secondBest] < fitness[y])
					&& (fitness[y] < fitness[firstBest])) {
				secondBest = y;
			}
		}
		
		// G

		for (int x = 0; x < 10; x++) {
			double minRange = 0, maxRange = 0;
			int counter;
			randomState = randDouble();
			// Ignoring the elites 
			if ((x == firstBest) || (x == secondBest)) {
				for (int o = 0; o < numVar; o++) {
					selection[selCounter][o] = population[x][o];
				}
				selCounter++;
				continue;
			}
			counter = 0;
			
			// Probablistic selection based on the normalized set
			while (!((minRange < randomState) && (randomState <= maxRange))) {

				minRange = ranges[counter];
				maxRange = ranges[counter + 1];
				counter++;
				// System.out.println(minRange + "-->" + maxRange);
			}
			for (int w = 0; w < numVar; w++) {
				selection[selCounter][w] = population[counter - 1][w];
			}
			selCounter++;
			
	// Testing Stage START- IGNORE
			// System.out.println(randomState);
			// System.out.println("Counter: " + counter);
			// System.exit(0);

			// if (!((counter - 1) == firstBest) || ((counter - 1) ==
			// secondBest)) {
			// for (int w = 0; w < numVar; w++) {
			// selection[selCounter][w] = population[counter - 1][w];
			// }
			// selCounter++;
			// } else {
			// x--;
			// }
		}

		/*
		 * for (int i = 0; i < 10; i++) { // for (int j = 0; j < numVar; j++) {
		 * System.out.print("Selection: " + numberOfClauseSatisfy(selection[i])
		 * + " ==> "); System.out.print("Population: " +
		 * numberOfClauseSatisfy(population[i]) + " ==> ");
		 * System.out.println("Fitness: " + fitness[i]);
		 * 
		 * // } }
		 */
	// Testing Stage OVER- IGNORE
		
		// Rearranging the Set, by putting the Elites on top
		int holderCounter = 2, fnsCounter = 0;
		for (int p = 0; p < 10; p++) {
			if ((p == firstBest) || (p == secondBest)) {
				for (int b = 0; b < numVar; b++) {
					holder[fnsCounter][b] = selection[p][b];
				}
				fnsCounter++;
			} else {
				for (int g = 0; g < numVar; g++) {
					holder[holderCounter][g] = selection[p][g];
				}
				holderCounter++;
			}
		}
		
	// Testing Stage -Ignore
		/*
		 * for (int i = 0; i < 10; i++) { // for (int j = 0; j < numVar; j++) {
		 * System.out.println("Holder: " + numberOfClauseSatisfy(holder[i])); //
		 * } } System.out.println("FB: " + firstBest + " --- " + "SB: " +
		 * secondBest); for (int i = 0; i < 10; i++) { for (int j = 0; j <
		 * numVar; j++) { selection[i][j] = holder[i][j]; }
		 * 
		 * }
		 */
		// selection = holder;

		// System.out.println("done with selection");
	// Testing Stage Ends -Ignore
	}

	/*
	 * Performs the cross over ignoring the elites
	 */
	public void crossover() {
		// Stating at 3, because 0,1 are elites
		for (int i = 3; i < 10; i += 2) {
			for (int j = 0; j < numVar; j++) {
				randomState = randDouble();
				if (randomState > 0.5) {	// Probability of picking
					newPop[i - 1][j] = selection[i][j];
				} else {
					newPop[i - 1][j] = selection[i - 1][j];
				}
				randomState = randDouble();
				if (randomState > 0.5) {  	// Probability of performing crossover
					newPop[i][j] = selection[i][j];
				} else {
					newPop[i][j] = selection[i - 1][j];
				}
			}
		}
		newPop[0] = selection[0];
		newPop[1] = selection[1];

		// System.out.println("done with cross");
	}

	/*
	 * Mutation process based on random chance
	 */
	public void mutation() {
		for (int i = 2; i < 10; i++) {
			randomState = randDouble();
			if (randomState > 0.5) {	// Probability based on random number
				for (int j = 0; j < numVar; j++) {
					randomState = randDouble();
					if (randomState > 0.5) { // change to mutate and change state
						newPop[i][j] = !newPop[i][j];
					}
				}
			}
		}
		// Changes in mutation brought to the population for next round
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < numVar; j++) {
				population[i][j] = newPop[i][j];
			}

		}
		// population = newPop;

		// System.out.println("done with mutation");
	}

	public void flip() {
		
		// Count the total flip in all files, to calculate AVG Flip.
	
		// boolean[][] flipHolder = new boolean[10][numVar];
		int totalBefore = 0, totalAfter = 0;
		int tempSat = 0;
		List<Integer> randomInts = new ArrayList<Integer>();
		for (int x = 0; x < numVar; x++) {
			randomInts.add(x);
		}
		// Randomly shuffling to get the bit to switch
		Collections.shuffle(randomInts);

		for (int p = 2; p < 10; p++) {
			for (int i = 0; i < numVar; i++) {

				totalBefore = numberOfClauseSatisfy(population[p]);

				population[p][randomInts.get(i)] = !population[p][randomInts
						.get(i)];
				totalFlips++;

				totalAfter = numberOfClauseSatisfy(population[p]);

				if ((totalAfter - totalBefore) < 0) {
					population[p][randomInts.get(i)] = !population[p][randomInts
							.get(i)];
					totalFlips--;
				}
			}
		}

		// System.out.println("done with flip");
	}
	
	/*
	 * Random Integer generator;
	 */

	public int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) - 1) + min;
		return randomNum;
	}

	/*
	 * Random generator to get a number between 0-1
	 */

	public double randDouble() {

		// Usually this should be a field rather than a method variable so
		// that it is not re-seeded every call.
		int min, max;
		min = 1;
		max = 99;
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		double randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum / 100;
	}
}
