/**
 * 
 */
package BackEnd;

/**
 * @author Jigar Doshi
 *
 */
public class Node {

	private boolean exists, state, negation;

	public Node(boolean exists, boolean state ,boolean negation) {

		this.exists = exists;
		this.state = state;
		this.negation = negation;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public boolean getState() {
		return this.state;
	}

	public boolean getExists() {
		return this.exists;
	}
	public void setNegation(boolean negation) {
		this.negation = negation;
	}

	public boolean getNegation() {
		return this.negation;
	}
	
}
